$data.Entity.extend('$todo.Types.ToDoEntry', {
	Id: { type: 'int', key: true, computed: true },
	Value: { type: 'string' },
	CreatedAt: { type: 'datetime' },
	ModifiedAt: { type: 'datetime' },
	Done: { type: 'bool' }
});

$data.EntityContext.extend('$todo.Types.ToDoContext', {
	ToDoEntries: { type: $data.Entity, elementType: $todo.Types.ToDoEntry }
});

$todo.context = new $todo.Types.ToDoContext({ name: 'webSql', databaseName: 'todo' });
$todo.context.onReady({
	success: updateView,
	error: function() {
		$todo.context = null;
		updateView();
	}
});